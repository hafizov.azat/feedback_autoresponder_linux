### Автоответчик на отзывы в ВБ ###

## Текущая версия программы 1.2 ##

## Первичная настройка программы ##

Находим в телеграм бота [@wb_fb_autoresponder_bot](https://t.me/wb_fb_autoresponder_bot)

Нажимаем **Старт** чтобы бот имел права отправить Вам уведомления в случае ошибки (при разлогиневании в ВБ) и данные по отзывам где оценка не равна 5. Бот пришлет ваш **telegram id**, данный id нам понадобятся дальше.... 

Так нужно сделать со **всех** telegram аккаунтов по которым хотите получать уведомления. 

![image](/instructions/img/bot_msg.JPG)

Подключаемся к серверу по **ssh**

Переходим в в домашний каталог

`cd ~/`

Сразу получаем root права

`sudo -s`

Скачиваем репозитории(`sudo apt install wget` если нет пакета wget)

`wget https://gitlab.com/hafizov.azat/feedback_autoresponder_linux/-/archive/main/feedback_autoresponder_linux-main.zip`

Распаковываем архив (`sudo apt install unzip` - если нет менеджера архивов)

`unzip feedback_autoresponder_linux-main.zip -d wb_feedback_autoresponder`

Удаляем архив (если захотите, чтобы не мусорить)

`rm feedback_autoresponder_linux-main.zip`

Переходим в директорию

`cd wb_feedback_autoresponder/feedback_autoresponder_linux-main/`

Скачиваем [WinSCP](https://winscp.net/eng/downloads.php) подключаемся к серверу проходим в директорию с программой и скачиваем файл **Ответы.xlsx**

![image](/instructions/img/exel_download.JPG)

Правим данные по telegram id которые мы ранее получали уведомлении, забиваем их в странице **“Телеграм”**. Так же можно вписать свой **bot api** в ячейке **B2** если Вы хотите получать уведомления в другом telegram боте.

![image](/instructions/img/tg_notif_set.JPG)

Так же нужно поправить в странице **“Рекомендации”** вариацию рекомендаций, если на один артикул несколько рекомендаций, программа будет выбирать рандомную вариацию

![image](/instructions/img/recommend.JPG)

Так же можно поправить страницу **“Ответ”**. В ответах допускаются следующие константы:

>> **$brand$** - подставиться бренд по артикулу с ВБ у текущего отзыва

>> **$articul$** - подставиться артикул с ВБ у текущего отзыва

>> **$seller$** - подставиться продавец у текущего артикула с ВБ

В столбе рекомендации допускаются следующие константы

>> **$orig_name$** - подставиться Название продукта (**первый столб**) со страницы “**Рекомендации”** по артикулу

>> **$recommend_name$** - подставиться имя рекомендуемого товара (**третий столб**) со страницы **“Рекомендации”** по артикулу

>> **$recommend_articul$** - подставиться артикул рекомендуемого товара (**четвертый столб**) со страницы **“Рекомендации”** по артикулу

![image](/instructions/img/answers.JPG)

Как подготовим файл сохранив его скидываем обратно на сервер

![image](/instructions/img/exel_upload.JPG)

Далее даем права на запуск программам **autoresponder.bin**, **autorisathion.bin** и **setup.bin**

`chmod a+x autoresponder.bin`

`chmod a+x autorisathion.bin`

`chmod a+x setup.bin`

Проходим авторизацию в ВБ

`./autorisathion.bin`

![image](/instructions/img/autorisation.JPG)

Придет уведомление в телеграм (если вы вписали правильно telegram idшники)

Устанавливаем службу

`./setup.bin`

Создается служба **WB_feedback_autoresponder.service**

Проверяем статус службы

`systemctl status WB_feedback_autoresponder.service`

**Процесс первичной настройки пройден!**

## Авторизация сессии в ВБ

При разалогинивании сессии в ВБ проходим авторизацию запустив **autorisathion.bin**

Проходим в каталог с программой 

`cd /opt/wb_feedback_autoresponder/wb_feedback_autoresponder-main`

Авторизовываемся запустив **autorisathion.bin**

`./autorisathion.bin`

Проходим стадию аутентификации как это делали при первичном запуске

![image](/instructions/img/autorisation.JPG)

Авторизация пройдена!

## Обновление программы
Скачеваем актуальные репозитории с [GitLab](https://gitlab.com/hafizov.azat/feedback_autoresponder_linux/-/archive/main/feedback_autoresponder_linux-main.zip) на локальный компьютер и разархивируем.

Подключаемся к серверу по [WinSCP](https://winscp.net/eng/downloads.php) и закидываем **3** файла (**autoresponder.bin**, **autorisathion.bin** и **setup.bin**) с скачанного архива на сервер с программой

Далее даем права на запуск программам autoresponder.bin, autorisathion.bin и setup.bin

`chmod a+x autoresponder.bin`

`chmod a+x autorisathion.bin`

`chmod a+x setup.bin`

![image](/instructions/img/bin_upload.JPG)

Перезапускаем службу

`systemctl restart WB_feedback_autoresponder.service`

**Программа обновлена!**
